<script src="../resources/js/jquery.min.js" type="text/javascript"></script>
<script src="../resources/js/bootstrap.min.js" type="text/javascript"></script>
<script src="../resources/plugins/data-tables/js/jquery.dataTables.js" type="text/javascript"></script>
<script src="../resources/plugins/fit-text/jquery.fittext.js" type="text/javascript"></script>
<script src="../resources/plugins/js-lists/jslists.js" type="text/javascript"></script>
<script>
    $( document ).ready(function() {
        //resize slogan on homepage based on browser
        $(".slogan").fitText(1.2, { maxFontSize: '24px' });

        //initialize tooltip
        $(function () {
            $('[data-toggle="tooltip"]').tooltip()
        })

        //add additional dependants on quote page
        $('#addPeople').on('click', function() {
            $('#additional').fadeToggle('slow', 'linear');
            $(this).toggleClass('fa-plus, fa-minus');
            $('.dep-box').removeClass('highlight');
        });

        //Get bootstrap modal when icons are clicked
        function getIns(){
            var parent = this.parentElement;
            $.ajax({
                type: "POST",
                url: "modal.php",
                data: {id: parent.id},
                dataType: "html",
                success: function (data) {
                    $('#modal-wrapper').html(data);
                    $('#myModal').modal('show');
                }
            });
        }
        var btn = document.getElementsByClassName('insType');
        for(i=0; i < btn.length; i++) {
            btn.item(i).addEventListener('click', getIns);
        }

        //fade out flash message after 5 seconds
        //$('.alert').delay(5000).fadeOut(1000);

        var monkeyList = new List('test-list', {
            valueNames: ['name'],
            page: 5,
            pagination: true
        });
    });
</script>