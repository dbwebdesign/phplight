<div class="row">
    <div class="col-md-12 text-center">
        <ul class="company">
            <li id="1"><button class="insType"><img src="/resources/img/companies/aetna.jpg" alt="Aetna Medical Insurance"></button></li>
            <li id="2"><button class="insType"><img src="/resources/img/companies/ars.jpg" alt="Azimuth Risk Solutions"></button></li>
            <li id="3"><button class="insType"><img src="/resources/img/companies/cigna.jpg" alt="Cigna"></button></li>
<!--            <li id="4"><button class="insType"><img src="/resources/img/companies/gbg.jpg" alt="Global Benefits Group"></button></li>-->
            <li id="5"><button class="insType"><img src="/resources/img/companies/geoblue-logo.jpg" alt="Geo Blue"></button></li>
            <li id="6"><button class="insType"><img src="/resources/img/companies/hcc.jpg" alt="HCC"></button></li>
            <li id="7"><button class="insType"><img src="/resources/img/companies/hth.jpg" alt="HTH Worldwide"></button></li>
            <li id="8"><button class="insType"><img src="/resources/img/companies/img.jpg" alt="IMG"></button></li>
            <li id="9"><button class="insType"><img src="/resources/img/companies/metlife.jpg" alt="Met Life"></button></li>
            <li id="10"><button class="insType"><img src="/resources/img/companies/seven-corners.jpg" alt="Seven Corners"></button></li>
        </ul>
    </div>
    <div id="modal-wrapper"></div>
</div>
