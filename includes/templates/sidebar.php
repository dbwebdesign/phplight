<div class="col-md-3 sidebar">
<ul class="list-unstyled clear-margins"><!-- widgets -->
    <li class="widget-container widget_nav_menu"><!-- widgets list -->
        <h1 class="title-widget">EASY AS 1-2-3!</h1>
        <ul>
            <li><a  href="quote.php"><i class="fa fa-angle-double-right"></i>  Instant Comparative Quotes</a></li>
            <li><a  href="contact.php"><i class="fa fa-angle-double-right"></i>  Contact Us</a></li>
            <li><a  href="apply-now.php"><i class="fa fa-angle-double-right"></i>  Apply Online</a></li>
        </ul>
    </li>
</ul>
<br>
<ul class="list-unstyled clear-margins"><!-- widgets -->
    <li class="widget-container widget_nav_menu"><!-- widgets list -->
        <h1 class="title-widget">Important Info</h1>
        <ul>
            <li><a href="resources/img/mistakes.pdf" target="_blank"><i class="fa fa-angle-double-right"></i> 4 Mistakes To
                    Avoid</a></li>
            <li><a href="advantages.php"><i class="fa fa-angle-double-right"></i> Our Advantages</a></li>
            <li><a href="how-to-use.php"><i class="fa fa-angle-double-right"></i> How To Use</a></li>
            <li><a href="services.php"><i class="fa fa-angle-double-right"></i> Our Services</a></li>
            <li><a href="testimonials.php"><i class="fa fa-angle-double-right"></i> Testimonials</a></li>
        </ul>
    </li>
</ul>
<br>
<ul class="list-unstyled clear-margins"><!-- widgets -->
    <li class="widget-container widget_nav_menu"><!-- widgets list -->
        <h1 class="title-widget">Going To France?</h1>
        <ul>
            <li><a href="france.php"><img class="france" src="resources/img/france.gif"> Click here for Advice on the
                    carte de s&eacute;jour</a></li>
        </ul>
    </li>
</ul>
<br>
<ul class="list-unstyled clear-margins"><!-- widgets -->
    <li class="widget-container widget_recent_news"><!-- widgets list -->
        <h1 class="title-widget">Connect With Us</h1>
        <div class="social-icons text-center">
            <ul class="nomargin">
                <a href="https://www.facebook.com/Global-Insurance-Net-105821432782565/" target="_blank"><i class="fa fa-facebook-square fa-3x social-fb" id="social"></i></a>
                <a href="https://twitter.com/global_expat" target="_blank"><i class="fa fa-twitter-square fa-3x social-tw" id="social"></i></a>
                <a href="https://plus.google.com/101550238345021951274" target="_blank"><i class="fa fa-google-plus-square fa-3x social-gp" id="social"></i></a>
                <a href="mailto:info@globalinsurancenet.com"><i class="fa fa-envelope-square fa-3x social-em" id="social"></i></a>
            </ul>
        </div>
    </li>
</ul>
</div>