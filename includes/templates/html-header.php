<!DOCTYPE html>
<html>
<head>
    <title>Global Insurance Net</title>
    <link rel="stylesheet" href="resources/css/bootstrap.css">
    <link rel="stylesheet" href="resources/css/bootstrap-theme.css">
    <link rel="stylesheet" href="resources/plugins/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="resources/plugins/data-tables/css/jquery.dataTables.css">
    <link rel="stylesheet" href="resources/css/main.css">
    <?php include('scripts.php'); ?>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
</head>
<body>
<div class="wrapper">