<!--footer-->
<footer class="footer1">
    <div class="container">

        <div class="row"><!-- row -->

            <div class="col-lg-4 col-md-4"><!-- widgets column left -->
                <ul class="list-unstyled clear-margins"><!-- widgets -->
                    <li class="widget-container widget_nav_menu"><!-- widgets list -->
                        <h1 class="title-widget">GLOBAL HEALTH INSURANCE</h1>
                        <ul>
                            <li><a href="quote.php"><i class="fa fa-angle-double-right"></i> Instant Comparative Quotes</a></li>
                            <li><a href="how-it-works.php"><i class="fa fa-angle-double-right"></i> How It Works</a></li>
                            <li><a href="apply-now.php"><i class="fa fa-angle-double-right"></i> Apply Now</a></li>
                        </ul>
                    </li>
                </ul>
            </div><!-- widgets column left end -->

            <div class="col-lg-4 col-md-4"><!-- widgets column left -->
                <ul class="list-unstyled clear-margins"><!-- widgets -->
                    <li class="widget-container widget_nav_menu"><!-- widgets list -->
                        <h1 class="title-widget">ABOUT US</h1>
                        <ul>
                            <li><a href="services.php"><i class="fa fa-angle-double-right"></i> Our Services</a></li>
                            <li><a href="team.php"><i class="fa fa-angle-double-right"></i> Our Team</a></li>
                            <li><a href="testimonials.php"><i class="fa fa-angle-double-right"></i> Testimonials</a></li>
                        </ul>
                    </li>
                </ul>
            </div><!-- widgets column left end -->

            <div class="col-lg-4 col-md-4"><!-- widgets column center -->
                <ul class="list-unstyled clear-margins"><!-- widgets -->
                    <li class="widget-container widget_recent_news"><!-- widgets list -->
                        <h1 class="title-widget">Contact Us </h1>
                        <div class="footerp">
                            <h2 class="title-median">Global Insurance Consultants, Inc.</h2>
                            <p><b>Email:</b> <a href="mailto:info@globalinsurancenet.com">info@globalinsurancenet.com</a></p>
                            <p><b>Phone:</b>
                                <b style="color:#f0ab48;">(9AM to 5PM EST):</b>1-305-661-6793</p>
<!--                            <p><b>Fax:</b>1-305-675-6134</p>-->
<!--                            <p><b>Address:</b><br>-->
<!--                                8950 SW 74th Ct. Suite 2201 A-21<br>-->
<!--                                Miami, FL 33156, USA-->
<!--                            </p>-->
                        </div>

<!--                        <div class="social-icons">-->
<!---->
<!--                            <ul class="nomargin">-->
<!--                                <a href="https://www.facebook.com/bootsnipp"><i class="fa fa-facebook-square fa-3x social-fb" id="social"></i></a>-->
<!--                                <a href="https://twitter.com/bootsnipp"><i class="fa fa-twitter-square fa-3x social-tw" id="social"></i></a>-->
<!--                                <a href="https://plus.google.com/+Bootsnipp-page"><i class="fa fa-google-plus-square fa-3x social-gp" id="social"></i></a>-->
<!--                                <a href="mailto:bootsnipp@gmail.com"><i class="fa fa-envelope-square fa-3x social-em" id="social"></i></a>-->
<!--                            </ul>-->
<!--                        </div>-->
                    </li>
                </ul>
            </div>
        </div>
    </div>
</footer>
<!--header-->
<div class="footer-bottom">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                <div class="copyright">
                    &copy; <?php echo date('Y') ?> Global Insurance Consultants, Inc. | All rights reserved
                </div>
            </div>
        </div>
    </div>
</div>

</div><!--End Of Wrapper-->
</body>
</html>