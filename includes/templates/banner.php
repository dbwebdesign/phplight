<?php include ('data/countries.php'); ?>
<div class="jumbotron">
    <div class="container banner">
        <div class="row">
            <div class="col-md-8">
                <div class="slogan">The #1 International Health Insurance Comparison Site.</div>
            </div>
            <div class="col-md-4">
                <form action="quote.php" method="post">
                    <div class="panel panel-warning">
                        <div class="panel-heading">
                            <h3 class="panel-title"><i>Get Instant Comparative Quotes!</i></h3>
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <select name="country" class="form-control">
                                            <option value="">Select A Country</option>
                                            <?php do { ?>
                                                <option value="<?php echo  $row_country['id']; ?>"><?php echo  $row_country['name']; ?></option>
                                            <?php } while ($row_country = mysqli_fetch_assoc($countries)); ?>
                                            <?php   mysqli_free_result($countries); ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-9 gender">
                                    <div class="form-group">
                                        <select name="gender" class="form-control">
                                            <option value="">Select Gender</option>
                                            <option value="male">Male</option>
                                            <option value="female">Female</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <input type="text" name="age" class="form-control" placeholder="Age">
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class='deductible-title'>
                                        Select Deductible Level(s)
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="checkbox">
                                        <label class="checkbox-inline">
                                            <input type="checkbox" name="deductible1" value="250"> 250
                                        </label>
                                        <label class="checkbox-inline">
                                            <input type="checkbox" name="deductible3" value="1000"> 1000
                                        </label>
                                        <label class="checkbox-inline">
                                            <input type="checkbox" name="deductible5" value="2500"> 2500
                                        </label>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="checkbox">
                                        <label class="checkbox-inline">
                                            <input type="checkbox" name="deductible2" value="500"> 500
                                        </label>
                                        <label class="checkbox-inline">
                                            <input type="checkbox" name="deductible4" value="2000"> 2000
                                        </label>
                                          <label class="checkbox-inline">
                                            <input type="checkbox" name="deductible6" value="5000"> 5000
                                        </label>
                                    </div>
                                </div>

                                <div class="col-md-12" style="border-top: 1px solid #ccc; text-align: center;">
                                    <input type="checkbox" name="dependants"> Check box to include partner/spouse
                                </div>
                            </div>
                        </div>
                        <div class="panel-footer clearfix">
                            <div class="pull-left" style="width: 70%; font-size: 12px;">
                                Please ask us to help you select the proper plan
                                for your needs and budget goals.
                            </div>
                            <button name="banner" type="submit" class="btn btn-warning pull-right">Get Quote</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>