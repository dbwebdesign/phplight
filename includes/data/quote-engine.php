<?php
    include('config/db-connect.php');
    $list = [];
    //Assign post data to variables
    $country = (isset($_POST['country']) ? $_POST['country'] : '');
    $gender = (isset($_POST['gender']) ? $_POST['gender'] : '');
    $age = (isset($_POST['age']) ? $_POST['age'] : '');
    $addGender = (isset($_POST['addGender']) ? $_POST['addGender'] : '');
    $addAge = (isset($_POST['addAge']) ? $_POST['addAge'] : '');

    //Create an array for deductibles checked to enter into MySQL query
    $deductible = array(
        (isset($_POST['deductible1']) ? $_POST['deductible1'] : 0),
        (isset($_POST['deductible2']) ? $_POST['deductible2'] : 0),
        (isset($_POST['deductible3']) ? $_POST['deductible3'] : 0),
        (isset($_POST['deductible4']) ? $_POST['deductible4'] : 0),
        (isset($_POST['deductible5']) ? $_POST['deductible5'] : 0),
        (isset($_POST['deductible6']) ? $_POST['deductible6'] : 0));

    //Set the checked deductibles on the quote page
    $deductible1 = (isset($_POST['deductible1']) ? "'checked', true" : "'checked', false");
    $deductible2 = (isset($_POST['deductible2']) ? "'checked', true" : "'checked', false");
    $deductible3 = (isset($_POST['deductible3']) ? "'checked', true" : "'checked', false");
    $deductible4 = (isset($_POST['deductible4']) ? "'checked', true" : "'checked', false");
    $deductible5 = (isset($_POST['deductible5']) ? "'checked', true" : "'checked', false");
    $deductible6 = (isset($_POST['deductible6']) ? "'checked', true" : "'checked', false");
    $dependants = (isset($_POST['dependants']) ? 'true' : 'false');

    //Additional Persons
    if(isset($_POST['addGender']) && isset($_POST['addAge'])){
        $addPerson = [
            0 =>['gender' => $gender, 'age' =>$age],
            1 =>['gender' => $addGender, 'age' =>$addAge]
        ];
    }
?>
<script>
    //run script to populate data in form on quote page
    $( document ).ready(function() {
        //Main Persons Information
        $('[name=country]').val('<?php echo($country);?>');
        $('[name=gender]').val('<?php echo($gender);?>');
        $('[name=age]').val('<?php echo($age);?>');

        //Deductibles Selected
        $('[name=deductible1]').attr(<?php echo($deductible1);?>);
        $('[name=deductible2]').attr(<?php echo($deductible2);?>);
        $('[name=deductible3]').attr(<?php echo($deductible3);?>);
        $('[name=deductible4]').attr(<?php echo($deductible4);?>);
        $('[name=deductible5]').attr(<?php echo($deductible5);?>);
        $('[name=deductible6]').attr(<?php echo($deductible6);?>);

        //add partner selected
        $('[name=addGender]').val('<?php echo($addGender);?>');
        $('[name=addAge]').val('<?php echo($addAge);?>');

    });
</script>

<?php

//check to see if the form on the banner was submitted
if(isset($_POST['banner'])){ ?>
    <script>
        //if dependant check box is selected show form and highlight it in red
        $( document ).ready(function() {
            var dependants = <?php echo $dependants; ?>;
            if(dependants){
                $('#additional').fadeToggle('slow', 'linear');
                $('#addPeople').toggleClass('fa-plus, fa-minus');
                $('.dep-box').addClass('highlight');
            }
        });
    </script>

    <?php
    //check form is filled out
    // Check if gender has been entered
    if ($_POST['gender'] == '') {
        $errGender = 'Please select a gender';
    }

    // Check if age has been entered
    if ($_POST['age'] == '') {
        $errAge = 'Enter Age';
    }
    // Check if deductible has been entered
    if (!$_POST['deductible1'] && !$_POST['deductible2'] && !$_POST['deductible3'] && !$_POST['deductible4'] && !$_POST['deductible5'] && !$_POST['deductible6']) {
        $errDed = 'Please select at least one deductible';
    }

    //if dependants checkbox is not selected run query to populate results
    if(!isset($_POST['dependants']) && !$errGender && !$errAge && !$errDed){
        $deductible = implode($deductible, ', ');
        $sql = "SELECT name, deductible, $gender
                FROM rates r
                left join companies c on c.id=r.company_id
                where $age >= age1 and $age <= age2 and deductible in ($deductible)
                group by r.company_id, r.deductible
                ";
        $res = mysqli_query($conn, $sql);

        while ($r = mysqli_fetch_object($res)) {
            $list[$r->name][$r->deductible][$gender] = $r->$gender;
        }
    ?>

        <script>
            $( document ).ready(function() {
                var quotes = $('#quotes');
                quotes.fadeIn("slow", function() {
                    $(this).removeClass("hide");
                });
                $('html, body').animate({
                    scrollTop: $('#scrollTo').offset().top
                }, 1000);
            });
        </script>
    <?php }elseif(isset($errGender)){

    } else{ ?>
        <script>
            $( document ).ready(function() {
            var quotes = $('#quotes');
            quotes.html('<h1 class="text-center" style="color:red; text-decoration: underline;">Please Add Your Spouse/Partner</h1>');
            $('html, body').animate({
                scrollTop: $('#dep-box').offset().top
            }, 1000);
            quotes.removeClass("hide");
            });
        </script>
    <?php } ?>
<?php } ?>


<?php
if(isset($_POST['quote'])){ ?>

    <?php
    //check form is filled out
    // Check if gender has been entered
    if ($_POST['gender'] == '') {
        $errGender = 'Please select a gender';
    }

    // Check if age has been entered
    if ($_POST['age'] == '') {
        $errAge = 'Enter Age';
    }
    // Check if deductible has been entered
    if (!$_POST['deductible1'] && !$_POST['deductible2'] && !$_POST['deductible3'] && !$_POST['deductible4'] && !$_POST['deductible5'] && !$_POST['deductible6']) {
        $errDed = '-- Please select at least one deductible';
    }

    $deductible = implode($deductible, ', ');
    $m = 1;
    $f = 1;
    if(!$errGender && !$errAge && !$errDed){
    foreach($addPerson as $value){
        if($value['gender'] != '' || $value['age'] != ''){

            if($value['gender'] == 'male'){
                $name = $value['gender'].' '.$m;
                $m++;
            }elseif($value['gender'] == 'female'){
                $name = $value['gender'].' '.$f;
                $f++;
            }

            $gender = $value['gender'];
            $age = $value['age'];

            $sql = "SELECT name, deductible,$gender as '$name'
                FROM rates r
                left join companies c on c.id=r.company_id
                where $age >= age1 and $age <= age2 and deductible in ($deductible)
                group by r.company_id, r.deductible
                ";

            $res = mysqli_query($conn, $sql);

            while ($r = mysqli_fetch_object($res)) {
                $list[$r->name][$r->deductible][$name] = $r->$name;
            }
        }
    }
    ?>

    <script>
        $( document ).ready(function() {
            var quotes = $('#quotes');
            quotes.fadeIn("slow", function() {
                $(this).removeClass("hide");
            });
            $('html, body').animate({
                scrollTop: $('#scrollTo').offset().top
            }, 1000);
        });
    </script>
<?php }} ?>