<?php

require_once('config/db-connect.php');
// I don't have a Products table so we'll use this one instead and get everything from it
$sql = "SELECT id, name FROM countries";
// runs the query and stores the results in the variable "$Products"
$countries = mysqli_query($conn, $sql) or die(mysqli_error());

// fetch the first record set from the query result and store in the variable $row_Products as an array
$row_country = mysqli_fetch_assoc($countries);

// next count the number of records in the record set and store the number in a variable
$totalRows_country = mysqli_num_rows($countries);

?>