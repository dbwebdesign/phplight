<?php
include('config/db-connect.php');

if ($_POST["submit"]) {
    $name = $_POST['name'];
    $country = $_POST['country'];
    $email = $_POST['email'];
    $message = $_POST['message'];
    $human = intval($_POST['human']);
    $insurance = '';

    foreach($_POST['ins'] as $ins => $value){
        if(isset($ins)){
            $insurance .= $value.', ';
        }
    }

    $headers= "From: $email";
    $to = 'info@globalinsurancenet.com';
    $subject = 'Message from Global Contact Form ';
    $body = "From: $name\nCountry: $country\nE-Mail: $email\nSelect Insurances: $insurance\nMessage: $message\n";

    // Check if name has been entered
    if (!$_POST['name']) {
        $errName = 'Please enter your name';
    }

    // Check if country has been entered
    if (!$_POST['country']) {
        $errCountry = 'Please enter your country of nationality';
    }

    // Check if email has been entered and is valid
    if (!$_POST['email'] || !filter_var($_POST['email'], FILTER_VALIDATE_EMAIL)) {
        $errEmail = 'Please enter a valid email address';
    }

    //Check if message has been entered
    if (!$_POST['message']) {
        $errMessage = 'Please enter your message';
    }
    //Check if simple anti-bot test is correct
    if ($human !== 5) {
        $errHuman = 'Your anti-spam is incorrect';
    }

// If there are no errors, send the email
    if (!$errName && !$errEmail && !$errMessage && !$errHuman) {
        if (mail($to, $subject, $body, $headers)) {

            //print success message on screen
            $result='<div class="alert alert-success">We will get back to you asap regarding your details below, typically within 24 hours.  Please check your "junk" email folder if you do not see our reply within 1-2 business days.</div>';

            //enter form data in database as backup
            $sql = "INSERT INTO contacts(`name`, `country`, `email`, `message`)
            VALUES('".$name."', '".$country."', '".$email."', '".$message."');
            ";
            $conn->query($sql);

            //clear the form fields
            $name = '';
            $country = '';
            $email = '';
            $message = '';
        } else {
            $result='<div class="alert alert-danger">Sorry there was an error sending your message. Please check the message and try again. If you continue to get errors please call us at 1-305-661-6793</div>';
        }
    }
}
?>