<?php

require_once('config/db-connect.php');
// I don't have a Products table so we'll use this one instead and get everything from it
$sql = "SELECT quote, name FROM testimonials";
// runs the query and stores the results in the variable "$Products"
$testimonials = mysqli_query($conn, $sql) or die(mysqli_error());

// fetch the first record set from the query result and store in the variable $row_Products as an array
$row_testimonials = mysqli_fetch_assoc($testimonials);

// next count the number of records in the record set and store the number in a variable
$totalRows_testimonials = mysqli_num_rows($testimonials);

?>