<?php
//Create connection
include_once('config/db-connect.php');

//Set database name
$db_name = 'example';

// Create database
$sql = "CREATE DATABASE $db_name";
if ($conn->query($sql) === TRUE) {
    echo "Database created successfully";
} else {
    echo "Error creating database: " . $conn->error;
}

//Close connection
$conn->close();
?>