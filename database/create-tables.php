<?php
//Create connection - must add dbname to db-connect.php
include_once('config/db-connect.php');

//Set table name
$table_name = 'companies';

// sql to create table
$sql = "CREATE TABLE $table_name (
`id` INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
`name` VARCHAR(120) NOT NULL,
`sort_order` INT(2) UNSIGNED NOT NULL,
`created_at` TIMESTAMP
)";

if ($conn->query($sql) === TRUE) {
    echo "Table $table_name created successfully";
} else {
    echo "Error creating table: " . $conn->error;
}
echo "\n";
//---------------------------------------------------------------
//*********************END OF TABLE******************************
//---------------------------------------------------------------

//Set table name
$table_name = 'age_range';

// sql to create table
$sql = "CREATE TABLE $table_name (
`id` INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
`min_age` INT(3) UNSIGNED NOT NULL,
`max_age` INT(3) UNSIGNED NOT NULL
)";

if ($conn->query($sql) === TRUE) {
    echo "Table $table_name created successfully";
} else {
    echo "Error creating table: " . $conn->error;
}
echo "\n";
//---------------------------------------------------------------
//*********************END OF TABLE******************************
//---------------------------------------------------------------

//Close connection
$conn->close();
?>