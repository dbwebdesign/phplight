<?php
$heading = 'Home';
include('templates/master.php');
include('templates/banner.php');
?>

<div class="container  main-body">
    <div class="row">
        <div class="col-md-12 text-center">
            <h1 class="text-primary">International Health Insurance In An Easy And Objective Format.</h1>
            <p>
            We make it easy for US expatriates, non-US citizens in the US or abroad, international travelers to or from any country. <b style="color:#ffc106;"><i>Avoid making mistakes that can cost you thousands of dollars!<i></b> Many of our clients throughout the world have saved thousands of dollars by ordering their insurance through us. Others have been able to improve the quality of their coverage without increasing their premiums, simply by changing their plans with our help. You can too!
            </p>
        </div>

        <div class="col-md-4">
            <div class="iconbox">
                <div class="iconbox-icon">
                    <span>1</span>
                </div>
                <div class="featureinfo text-center">
                    <h4><a href="quote.php">Compare Instant Quotes</a></h4>
                    <p>
                        From the top insurance companies to find the best prices.
                    </p>
                </div>
            </div>
        </div>

        <div class="col-md-4">
            <div class="iconbox">
                <div class="iconbox-icon">
                    <span>2</span>
                </div>
                <div class="featureinfo text-center">
                    <h4><a href="contact.php">Contact Us </a></h4>
                    <p>
                        So that we can help you select the best health plan for your specific needs from an array of global medical plans designed for expats.
                    </p>
                </div>
            </div>
        </div>

        <div class="col-md-4">
            <div class="iconbox">
                <div class="iconbox-icon">
                    <span>3</span>
                </div>
                <div class="featureinfo text-center">
                    <h4><a href="apply-now.php">Apply</a></h4>
                    <p>
                        For high quality coverage that protects you anywhere in the world(U.S. included).
                    </p>
                </div>
            </div>
        </div>


        <?php include('templates/companies.php') ?>

        <div class="col-md-12">
            <div id="myCarousel" class="carousel slide" data-ride="carousel"  data-interval="8000">
              <!-- Wrapper for slides -->
              <div class="carousel-inner">

                <div class="item active">
                   <div class="carousel-caption">
                    <h3>Robert H., Paris, France</h3>
                    <p>
                    A hats off to the team for a very efficient handling of the whole process. Have lived and worked abroad off and on for a lot of years, and things don't usually work like that. Bravo to Mr. Perez for putting together a good group of people.
                    </p>
                  </div>
                </div><!-- End Item -->

                 <div class="item">
                   <div class="carousel-caption">
                    <h3>Babette B., Paris, France</h3>
                    <p>
                    We are so impressed with your prompt and professional response to my inquiry, as well as by your courtesy and obvious competence. Everyone has been telling us how difficult it is going to be to find an insurance plan for our needs... obviously they have not found YOU! Thank you again for your assistance and interest.
                    </p>
                  </div>
                </div><!-- End Item -->

                <div class="item">
                   <div class="carousel-caption">
                    <h3>Anthony M., Riyadh, Saudi Arabia</h3>
                    <p>
                    You have gone repeatedly above and beyond in providing a level of service that was quite unexpected. You have responded to every inquiry promptly and professionally and have added a human dimension to this whole process that is often overlooked in the course of modern business. I am very impressed, thankful, and felt that such quality deserves recognition and it is in this light I pass on my thoughts.
                    </p>
                  </div>
                </div><!-- End Item -->

                <div class="item">
                   <div class="carousel-caption">
                    <h3>Gadi K., Kullu, India</h3>
                    <p>
                    I'm fairly dumbfounded by how kind the insurance company has been to us. I've read so many horror stories about insurance companies but they have exemplified fair play. I would also like to take this opportunity to thank you personally. You are really incredible, taking off what could have been a huge burden off our shoulders. Thanks and thanks again.
                    </p>
                  </div>
                </div><!-- End Item -->
              </div><!-- End Carousel Inner -->
            </div><!-- End Carousel -->
        </div>
    </div>
</div>
<?php include('templates/footer.php'); ?>