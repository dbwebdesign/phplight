<?php
    $heading = 'Glossary';
    include('templates/master.php');
?>

<div class="container">
    <div class="row">
            <?php include('templates/sidebar.php'); ?>
        <div class="col-md-9">
            <div id="glossary">
                <h1 class="text-primary">Insurance Glossary</h1>
            </div>

            <!-- Nav tabs -->
            <ul class="nav nav-tabs" role="tablist">
                <li role="presentation" class="active"><a href="#A" aria-controls="A" role="tab" data-toggle="tab">A</a></li>
                <li role="presentation"><a href="#B" aria-controls="B" role="tab" data-toggle="tab">B</a></li>
                <li role="presentation"><a href="#C" aria-controls="C" role="tab" data-toggle="tab">C</a></li>
                <li role="presentation"><a href="#D" aria-controls="D" role="tab" data-toggle="tab">D</a></li>
                <li role="presentation"><a href="#E" aria-controls="E" role="tab" data-toggle="tab">E</a></li>
                <li role="presentation"><a href="#F" aria-controls="F" role="tab" data-toggle="tab">F</a></li>
                <li role="presentation"><a href="#G" aria-controls="G" role="tab" data-toggle="tab">G</a></li>
                <li role="presentation"><a href="#H" aria-controls="H" role="tab" data-toggle="tab">H</a></li>
                <li role="presentation"><a href="#I" aria-controls="I" role="tab" data-toggle="tab">I</a></li>
                <li role="presentation"><a href="#K" aria-controls="K" role="tab" data-toggle="tab">K</a></li>
                <li role="presentation"><a href="#L" aria-controls="L" role="tab" data-toggle="tab">L</a></li>
                <li role="presentation"><a href="#M" aria-controls="M" role="tab" data-toggle="tab">M</a></li>
                <li role="presentation"><a href="#P" aria-controls="P" role="tab" data-toggle="tab">P</a></li>
                <li role="presentation"><a href="#R" aria-controls="R" role="tab" data-toggle="tab">R</a></li>
                <li role="presentation"><a href="#S" aria-controls="S" role="tab" data-toggle="tab">S</a></li>
                <li role="presentation"><a href="#T" aria-controls="T" role="tab" data-toggle="tab">T</a></li>
                <li role="presentation"><a href="#U" aria-controls="U" role="tab" data-toggle="tab">U</a></li>
                <li role="presentation"><a href="#V" aria-controls="V" role="tab" data-toggle="tab">V</a></li>
                <li role="presentation"><a href="#W" aria-controls="W" role="tab" data-toggle="tab">W</a></li>
                <li role="presentation"><a href="#Y" aria-controls="Y" role="tab" data-toggle="tab">Y</a></li>
            </ul>

            <!-- Tab panes -->
            <div class="tab-content">
                <div role="tabpanel" class="tab-pane fade in active" id="A">
                    <p> <b>Accidental death and dismemberment rider</b><br>
                    An additional benefit rider or endorsement that provides for an amount
                    of money in addition to the death benefit of a life insurance policy.<br>
                    The additional amount is payable only if the insured dies or loses any
                    two limbs or the sight of both eyes as the result of an accident.</p>
                    <p><b>Accidental death benefit rider</b><br>
                    An additional benefit rider or endorsement that provides for an amount
                    of money in addition to the basic death benefit of a life insurance policy.
                    This additional amount is payable only if the insured dies as the result
                    of an accident.</p>
                    <p><b>Acturial department</b><br>
                    The department in a life and health insurance company responsible for
                    seeing that the company's operations are conducted on a mathematically
                    sound basis. </p>
                    <p><b>Actuary</b><br>
                    Technical expert in life insurance, particularly in mathematics. The person
                    in this job applies the theory of probability to calculate mortality rates,
                    morbidity rates, lapse rates, premium rates, policy reserves, and reserves,
                    and other values.</p>
                    <p><b>Agency System</b><br>
                    A distribution system in which insurance companies use their own commissioned
                    agents to sell and deliver insurance policies. </p>
                    <p><b>Annuity</b><br>
                    A series of payments made or received at regular intervals.</p>
                    <p><b>Annuity mortality table</b><br>
                    A tabulation of probabilities of dying at each age. Used by actuaries
                    to calculate premiums and reserves for annuities in which benefits are
                    paid only if a designated person is alive. </p>
                    <p><b>Application</b><br>
                    A form that must be completed by an individual or other party who is seeking
                    insurance coverage.</p>
                    <p><b>Assignment of benefits</b><br>
                    An authorization directing an insurer to make payment directly to a provider
                    of benefits, such as a physician or dentist, rather than to the insured.</p>
                    <p><b>Attending Physician's Statement (APS)</b><br>
                    A written statement from a physician who has treated, or is currently
                    treating, a proposed insured or an insured for one or more conditions.</p>
                    <p><b>Aviation exclusion</b><br>
                    A life insurance contract provision which specifies that the death benefit
                    is not payable if the insured dies as a result of certain aviation activities.</p>

                </div>
                <div role="tabpanel" class="tab-pane fade" id="B">
                    <p><b>Beneficiary</b><br>
                    The person or other party designed to receive life insurance policy proceeds.</p>
                    <p><b>Benefit</b><br>
                    The amount of money paid when an insurance claim is approved.</p>
                    <p><b>Blended Rates</b><br>
                    Group mortality rates that are based partially on a group's own experience
                    and partially on manual rates.</p>
                    <p><b>Blue Cross Plan</b><br>
                    A hospital expense insurance plan offered by a regionally operated health
                    care provider affiliated with a large national nonprofit health care organization.</p>
                    <p><b>Blue Shield plan</b><br>
                    A physician expense insurance plan offered by a regionally operated health
                    care provider affiliated with a large national nonprofit health care organization.</p>
                    <p><b>Business-continuation insurance</b><br>
                    A type of business insurance designed to provide funds so the remaining
                    partners in a business, or the remaining stockholders in a closely held
                    corporation, can buy the business interest of a deceased or disabled partner
                    or stockholder.</p>
                    <p><b>Business insurance</b><br>
                    Insurance that is intended to serve the insurance needs of a business
                    rather than the needs of an individual.</p>
                </div>
                <div role="tabpanel" class="tab-pane fade" id="C">
                    <p><b>Cash surrender value</b><br>
                    The amount of money adjusted for factors such as policy loans or late
                    premiums, that the policy owner will receive if the policy owner cancels
                    the coverage and surrenders the policy to the insurance company.</p>
                    <p><b>Cash Value</b><br>
                    Amount of money, before adjustment for factors such as policy loans or
                    late premiums that the policy owner will receive if the policy owner allows
                    the policy to lapse or cancels the coverage and surrenders the policy
                    to the insurance company.</p>
                    <p><b>Claim</b><br>
                    A request for repayment under the terms of an insurance policy.</p>
                    <p><b>Claim administration department</b><br>
                    Department in a life and health insurance company responsible for processing
                    claims. </p>
                    <p><b>Claimant</b><br>
                    The person or party making a formal request for payment of benefits due
                    under the terms of an insurance contract.</p>
                    <p><b>Claim examiner</b><br>
                    An employee of an insurance company whose responsibilities include investigating
                    claims, approving the claims that are valid and denying those that are
                    invalid or fraudulent.</p>
                    <p><b>Coinsurance</b><br>
                    A type of reinsurance plan in which the ceding company pays the reinsurer
                    part of the premium paid by the insured, minus a proportionate share of
                    the commission and premium taxes associated with the policy that is being
                    reinsured and a portion of the ceding company's general overhead expenses.</p>
                    <p><b>Commission</b><br>
                    The amount of money paid to an insurance agent for selling an insurance
                    policy. A commission is always calculated as a percentage of the premium.</p>
                    <p><b>Comprehensive major medical insurance</b><br>
                    A form of health insurance coverage that combines the features and benefits
                    of a hospital-surgical expense policy and the features and benefits of
                    a major medical policy.</p>
                    <p><b>Coordination of benefits clause</b><br>
                    Provision in a group health insurance policy specifying that benefits
                    will not be paid for amounts reimbursed by other group health insurers.</p>
                    <p><b>Customer service department</b><br>
                    Department in a life and health insurance company that is charged with
                    providing assistance to the company's policy owners, agents, and beneficiaries.</p>
                </div>
                <div role="tabpanel" class="tab-pane fade" id="D">
                    <p><b>Death Benefits</b><br>
                    Amount of money paid or due to be paid when a person insured under a life
                    insurance policy dies.</p>
                    <p><b>Deductible</b><br>
                    Flat amount that an insured must pay before the insurance company will
                    make any benefit payments under a health insurance policy.</p>
                    <p><b>Disability Income Insurance </b><br>
                    Type of health insurance designed to compensate insured people for a portion
                    of the income they lose because of a disabling injury or illness.</p>
                    <p><b>Distribution expense</b><br>
                    Expenses involved in making insurance products available to the general
                    public. </p>
                    <p><b>Double Indemnity</b><br>
                    Death benefit coverage that pays an additional benefit equal to the basic
                    death benefit of the policy if the insured's death is accidental.</p>
                </div>
                <div role="tabpanel" class="tab-pane fade" id="E">
                    <p> <b>Eligibility requirements</b><br>
                    Conditions a person must meet in order to be a participant in a group
                    life insurance, group health insurance, or retirement plan.</p>
                    <p><b>Evidence of insurability</b><br>
                    Proof that a person is an insurable risk.</p>
                    <p><b>Exclusions</b><br>
                    Losses for which an insurance policy does not provide benefits.</p>
                </div>
                <div role="tabpanel" class="tab-pane fade" id="F">
                    <p> <b>Foreign Corporation</b><br>
                    Company that is incorporated under the laws of another state. </p>
                    <p><b>Fraudulent Claim</b><br>
                    Type of claim that occurs when a claimant intentionally uses false information
                    in an attempt to collect policy proceeds.</p>
                    <p><b>Full-service plan</b><br>
                    Health insurance plan which pays in full the actual cost, if reasonable
                    and customary, of services received, rather than a specified maximum for
                    each service.</p>
                    <p><b>Face Amount</b><br>
                    Amount stated as payable at the death of the insured or in the case of
                    an annuity at the maturity of the contract.</p>
                    <p><b>Family deductible</b><br>
                    Single deductible which, when satisfied, relieves a family of the burden
                    of satisfying a deductible for each individual family member.</p>
                    <p><b>Fee Schedule</b><br>
                    Schedule or list of maximum benefits that will pay under a group medical
                    contract for certain listed procedures.</p>
                    <p><b>Field underwriting</b><br>
                    Occurs when an agent gathers pertinent information about the proposed
                    insured and reports that information on the application blank so the home
                    office underwriter can make an underwriting decision.</p>
                    <p><b>First-dollar coverage</b><br>
                    Medical expense insurance under which no deductible or coinsurance is
                    applicable to covered expenses.</p>
                    <p><b>First-year commission</b><br>
                    An amount paid to an insurance agent based on a policy's first annual
                    premium amount.</p>
                </div>
                <div role="tabpanel" class="tab-pane fade" id="G">
                    <p><b>General Agent</b><br>
                    An independent entrepreneur who is under contract to the insurer.</p>
                    <p><b>Grace Period</b><br>
                    Length of time (usually 31 days) after a premium is due and unpaid during
                    which the policy, including all riders, remains in force.</p>
                    <p><b>Group Insurance</b><br>
                    Insurance that provides coverage for several people under one contract,
                    called a master contract.</p>

                </div>
                <div role="tabpanel" class="tab-pane fade" id="H">
                    <p><b>Health insurance</b><br>
                    Insurance covering medical expenses or income loss resulting from injury
                    or sickness. Health insurance is a general category that includes many
                    different types of insurance coverage, including hospital confinement
                    insurance, hospital expense insurance, surgical expense insurance, major
                    medical insurance, disability income insurance, dental expense insurance,
                    prescription drug insurance, and vision care insurance.</p>
                    <p><b>Health maintenance organization</b><br>
                    Organization that provides comprehensive health care services for subscribing
                    members in a particular geographic area.</p>
                    <p><b>History statement</b><br>
                    Attending physician's statement concerning a specific health history admitted
                    by the proposed insured.</p>
                    <p><b>Hospital-surgical expense insurance</b><br>
                    Type of health insurance that provides benefits related directly to hospitalization
                    costs and associated medical expenses incurred by an insured for treatment
                    of a sickness or injury.</p>
                </div>
                <div role="tabpanel" class="tab-pane fade" id="I">
                    <p><b>Inspection report</b><br>
                    Report made by a consumer reporting agency concerning a proposed insured's
                    lifestyle, occupation, and economic standing. </p>
                    <p><b>Insurability statement</b><br>
                    Questionnaire that an insurer may ask an applicant to complete when a
                    considerable amount of time has elapsed between the time the application
                    is received and the time the policy is actually issued.</p>
                    <p><b>Insurable interest</b><br>
                    Condition in which the person applying for an insurance policy and the
                    person who is to receive the policy benefit will suffer an emotional or
                    financial loss if the event insured against occurs.</p>
                    <p><b>Insurance</b><br>
                    System of protection against loss in which a number of individuals agree
                    to pay certain sums of money, called premiums, to create a pool of money
                    which will guarantee that the individuals will be compensated for losses
                    cause by events such as fire, accident, illness or death.</p>
                    <p><b>Insurance agent</b><br>
                    Representative of an insurance company who sells insurance. </p>
                </div>
                <div role="tabpanel" class="tab-pane fade" id="K">
                    <p><b>Key-person insurance</b><br>
                    Life insurance purchased by a business on the life of a person whose continued
                    participation in the business is necessary to the firm's success and whose
                    death or disability would cause financial loss to the company.</p>
                </div>
                <div role="tabpanel" class="tab-pane fade" id="L">
                    <p><b>Lapse</b><br>
                    Termination of an insurance policy because premiums were not paid when
                    they came due.</p>
                    <p><b>Level premiums</b><br>
                    Premiums that remain the same each year that the life insurance policy
                    is in force.</p>
                    <p><b>Level term insurance</b><br>
                    Type of term insurance that provides a death benefit that remains the
                    same during the period specified.</p>
                </div>
                <div role="tabpanel" class="tab-pane fade" id="M">
                    <p><b> Major Medical Insurance</b><br>
                    Type of medical expense insurance that provides broad coverage for most
                    of the expenses associated with treating a covered illness or injury.</p>
                    <p><b>Managed care</b><br>
                    Name given to a broad spectrum of techniques by which insurance companies
                    attempt to reduce health care costs by participating in decisions concerning
                    the treatment given to those they insure.</p>
                    <p><b>Material Misrepresentation</b><br>
                    A misstatement by an applicant that is relevant to the insurer's acceptance
                    of the risk, because, if the truth had been known, the insurer would not
                    have issued the policy or would have issued the policy on a different
                    basis.</p>
                    <p><b>Medicaid</b><br>
                    Government-funded program in the United States that provides medical expense
                    coverage for eligible people under age 65 who are indigent and meet certain
                    other criteria.</p>
                    <p><b>Medical application</b><br>
                    Application for insurance in which the proposed insured is required to
                    undergo some type of medical examination.</p>
                    <p><b>Medical expense insurance</b><br>
                    Types of health insurance designed to pay for part or all of an insured's
                    health care expenses, such as, hospital room and board, surgeon's fee,
                    visits to the doctors' offices, prescribed drugs, treatments and nursing
                    care.</p>
                    <p><b>Mode of premium payment</b><br>
                    Frequency with which premiums are paid for example, annually, quarterly,
                    monthly.</p>
                    <p><b>Mortality charge</b><br>
                    Cost of insurance protection element of a universal life policy. This
                    cost is based on the net amount at risk under the policy, the insured's
                    risk classification at the time of policy purchase, and the insured's
                    current age.</p>
                    <p><b>Mutual Insurance Company</b><br>
                    Insurance company owned by policy owners rather than stockholders.</p>

                    <p><b> Medical report</b><br>
                    Report on a proposed insured's health that is completed by a physician
                    and is based on a physical examination and questioning of the proposed
                    insured.</p>
                    <p><b>Medicare</b><br>
                    United States government program that provides medical expense coverage
                    to persons age 65 and over and to people with certain disabilities, as
                    specified by Congress.</p>
                </div>
                <div role="tabpanel" class="tab-pane fade" id="P">
                    <p><b>Paramedical Report</b><br>
                    Report based on a physical examination and a medical history completed
                    by a medical technician, a physician's assistant, or a nurse, rather than
                    a physician.</p>
                    <p><b>Partnership insurance</b><br>
                    Type of business insurance designed to provide funds so the remaining
                    partners in a business can buy the business interest of a deceased or
                    disabled partner.</p>
                    <p><b>Policy</b><br>
                    A written document that serves as evidence of an insurance contract and
                    contains the pertinent facts about the policy owner, the insurance coverage,
                    the insured, and the insurer.</p>
                    <p><b>Policy anniversary</b><br>
                    The anniversary of the date on which a policy was issued. </p>
                    <p><b>Policy charge</b><br>
                    An amount that an insurer adds to the gross premium to help cover the
                    insurer's expenses.</p>
                    <p><b>Policy owner</b><br>
                    Person or party who owns an individual insurance policy. The policy owner
                    is not necessarily the person whose life is insured.</p>
                    <p><b>Policy proceeds</b><br>
                    Amount that the beneficiary actually receives from a life insurance policy
                    after adjustments have been made to the basic death benefit for policy
                    loans, dividends, paid-up additions, late premium payments, and supplementary
                    benefit rider.</p>
                    <p><b>Pre-exisiting conditions</b><br>
                    Individual health insurance, an injury that occurred or sickness that
                    first manifested itself the policy was issued and that was not disclosed
                    on the application.<br>
                    In group health insurance, a condition for which an individual received
                    medical care during a specified period, usually three months, immediately
                    prior to the effective date of coverage.</p>
                    <p><b>Pre-exisiting conditions provision</b><br>
                    Provision in most medical expense insurance policy stating that until
                    the insured has been covered under the policy for a certain period, the
                    insurer will not pay benefits for any pre-existing condition.</p>
                    <p><b>Premium</b><br>
                    Payments or one of a series of payments, required by the insurer to put
                    an insurance policy in force and keep it in force.</p>
                    <p> <b>Primary beneficiary</b><br>
                    Party or parties who have first rights to receive policy benefits when
                    the benefits of an insurance policy become payable.</p>
                    <p><b>Primary provider of benefits</b><br>
                    Medical expense plan that pays the full benefits provided by its plan
                    before any benefits are paid by another medical expense plan.</p>
                </div>
                <div role="tabpanel" class="tab-pane fade" id="R">
                    <p> <b>Rated policy</b><br>
                    Policy issued to insure a person classified as having a greater-than-average
                    likelihood of loss. Policy may be issued with special exclusions, with
                    a premium rate that is higher than the rate for a standard policy or with
                    exclusions and a higher than standard premium rate.</p>
                    <p><b>Reasonable and customary charge</b><br>
                    Amount of money most frequently charged for certain medical procedures
                    in a given geographical area. </p>
                    <p><b>Reserve</b><br>
                    Liability account that identifies the amount of assets needed to pay future
                    claims. </p>
                    <p><b>Revocable beneficiary</b><br>
                    A named beneficiary whose right to life insurance policy proceeds is not
                    vested during the insured's lifetime and whose designation as beneficiary
                    can be cancelled by the policy owner at any time prior to the insured's
                    death.</p>
                    <p><b>Rider</b><br>
                    Amendment to an insurance policy that becomes a part of the insurance
                    contract and expands or limits the benefits payable.</p>
                    <p><b>Risk Class</b><br>
                    Group of insureds who present a substantially similar risk to the insurance
                    company. </p>
                </div>
                <div role="tabpanel" class="tab-pane fade" id="S">
                    <p> <b>Stock insurance company</b><br>
                    An insurance company that is owned by people who buy shares of the company's
                    stock.</p>
                    <p><b>Stop-loss provision</b><br>
                    Health insurance policy provision specifying that the insurer will pay
                    100 percent of the insured's eligible medical expenses after the insured
                    has incurred a specified amount of out-of-pocket expenses under the coinsurance
                    feature.</p>
                    <p><b>Substandard Premium rate</b><br>
                    Premium rate charged for insurance on an insured person classified as
                    having a grater than average likelihood of loss. </p>
                    <p><b>Substandard risk class</b><br>
                    Risk class made up of people with medical or non-medical impairments that
                    give them a greater than average likelihood of loss.</p>
                    <p><b>Suicide Clause</b><br>
                    Life insurance policy wording which specifies that the proceeds of the
                    policy will not be paid if the insured takes his or her own life within
                    a specified period of time (usually two years) after the policy's date
                    of issue.</p>
                    <p><b>Surrender charge</b><br>
                    Option of withdrawing your money from a whole, universal, or variable
                    universal life insurance policy. When you withdraw funds, you pay a surrender
                    charge for early withdrawal. The fee is expressed as a percentage of the
                    amount of the amount withdrawn.</p>

                </div>
                <div role="tabpanel" class="tab-pane fade" id="T">
                    <p> <b>Term Insurance</b><br>
                    Life insurance under which the benefit is payable only if the insured
                    dies during a specified period of time.</p>
                    <p><b>Third-party administrator</b><br>
                    Organization that administers an insurance contract for a self-insured
                    group but that does not have financial responsibility for paying claims.
                    Self-insured group pays its own claims.</p>
                    <p><b>Travel accident benefit</b><br>
                    Accidental death benefit often included in group insurance policies issued
                    to employer-employee groups. This benefit is payable only if an accident
                    occurs while an employee is traveling for the employer.</p>
                </div>
                <div role="tabpanel" class="tab-pane fade" id="U">
                    <p><b>Underwriter</b><br>
                    Person or organization that guarantees that money will be available to
                    pay for losses that are insured against. In this sense, the insurance
                    company is the underwriter.</p>
                    <p><b>Underwriting</b><br>
                    Process of assessing and classifying the potential degree of risk that
                    a proposed insured represents. </p>
                    <p><b>Underwriting department</b><br>
                    Department in a life and health insurance company that selects the risk
                    that the company will insure. Underwriting department tries to make sure
                    that the actual mortality or morbidity rates of the company's insureds
                    do not exceed the rates assumed when premium rates were calculated.</p>
                    <p><b>Underwriting requirements</b><br>
                    Instructions that indicate what evidence of insurability is required for
                    a given situation and which of several optional information sources will
                    be needed to provide underwriters with necessary information.</p>
                    <p><b>Uninsurable risk class</b><br>
                    Group of people with a risk of loss so great that an insurance company
                    will not offer them insurance.</p>
                </div>
                <div role="tabpanel" class="tab-pane fade" id="V">
                    <p> <b>Variable life insurance</b><br>
                    Form of whole life insurance under which the death benefit and the cash
                    value of the policy fluctuate according to the investment performance
                    of a separate account fund.</p>
                    <p><b>Variable universal life insurance </b><br>
                    Form of whole life insurance that combines the premium and death benefit
                    flexibility of universal life insurance with the investment flexibility
                    and risk of variable life insurance.</p>
                </div>
                <div role="tabpanel" class="tab-pane fade" id="W">
                    <p><b> War exclusion provision</b><br>
                    Life insurance policy provision that limits an insurer's liability to
                    pay a death benefit if the insured's death is connected with war or military
                    service.</p>
                    <p><b>Whole life insurance</b><br>
                    Life insurance that remains in force during the insured's entire lifetime,
                    provided premiums are paid as specified in the policy. Whole life insurance
                    also builds a savings elements called the cash value as a result of the
                    level premium approach to funding the death benefit.</p>
                </div>
                <div role="tabpanel" class="tab-pane fade" id="Y">
                    <p><b>Yearly renewable term insurance</b><br>
                    Term life insurance that gives the policy owner the right to continue
                    the coverage at the end of each year. This renewal right continues for
                    a specified number of years or until the insured reaches the age specified
                    in the contract. </p>
                </div>
            </div>
        </div>
    </div>
</div>

<?php include('templates/footer.php'); ?>