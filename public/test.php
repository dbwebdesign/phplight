<?php
$heading = 'Template';
include('templates/master.php');
?>

<div class="container">
    <div class="row">
        <?php include('templates/sidebar.php'); ?>
        <div class="col-md-9">
            <?php
            include "config/db-connect.php";

            $sql = "select `250`, `500`, `1000`
                    from homestead.seven_corners
                    where `range` between 30 and 39
                ";
            $res = mysqli_query($conn, $sql);

            $list = array();
            while ($r = mysqli_fetch_object($res)) {
                $list[$r->name][$r->deductible]['male'] = $r->male;
                $list[$r->name][$r->deductible]['female'] = $r->female;
                $list[$r->name][$r->deductible]['child'] = $r->female;
            }
            ?>

            <?php foreach ($list as $company => $rates) {?>
            <div class="panel panel-warning">
                <div class="panel-heading">
                    <h3 class="panel-title"><?php echo $company; ?></h3>
                </div>
                <div class="panel-body">
                    <?php foreach ($rates as $ratesId => $ratesInfo) { ?>
                        <div class="col-md-3">
                            <ul class="list-group">
                                <li class="list-group-item list-group-item-warning">Deductible: <?php echo $ratesId; ?></li>
                                <li class="list-group-item">Male: <?php echo $ratesInfo['male']; ?></li>
                                <li class="list-group-item">Female: <?php echo $ratesInfo['female']; ?></li>
                                <li class="list-group-item">Per Child: <?php echo $ratesInfo['child']; ?></li>
                            </ul>
                        </div>
                    <?php } ?>
                </div>
            </div>
            <?php } ?>

        </div>
    </div>
</div>

<?php include('templates/footer.php'); ?>