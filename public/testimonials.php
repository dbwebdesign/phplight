<?php
    $heading = 'Testimonials';
    include('templates/master.php');
    include('data/testimonials.php');
?>

<div class="container">
    <div class="row">
            <?php include('templates/sidebar.php'); ?>
        <div class="col-md-9">
            <div id="test-list">
                <ul class="list">
                    <?php do { ?>
                    <li>
                        <div class="box name">
                            <div class="box-body testimonials">
                                    <p><i class="fa fa-quote-left" aria-hidden="true"></i> <?php echo  $row_testimonials['quote']; ?></p>
                                    <h1 class="text-primary"><?php echo  $row_testimonials['name']; ?></h1>
                            </div>
                        </div>
                    </li>
                    <?php } while ($row_testimonials = mysqli_fetch_assoc($testimonials)); ?>
                    <?php   mysqli_free_result($testimonials); ?>
                </ul>
                <div class="text-right"><ul class="pagination"></ul></div>

            </div>
        </div>
    </div>
</div>

<?php include('templates/footer.php'); ?>