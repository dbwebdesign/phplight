<?php
    $heading = 'Our Company';
    include('templates/master.php');
?>

<div class="container">
    <div class="row">
            <?php include('templates/sidebar.php'); ?>
        <div class="col-md-9">
            <h1 class="text-primary">What's Our Mission?</h1>
            <p>Simply put: To provide you with low-cost access to the top insurance tools and products available. Whether you are looking for international health insurance or international life insurance, Global Insurance Consultants will help you find the best solution, at the lowest possible price.</p>

            <h1 class="text-primary">Who are we?</h1>
            <p>Global Insurance Consultants, Inc.is an independent, closely held insurance agency based in Miami, specializing in top-quality international health and life Insurance plans for expatriates and non-US residents, living all over the world. We are not owned, employed, or controlled by any insurance company, so we can remain totally independent and objective at all times. Currently, Global Insurance Consultants, Inc. and its sister company, Clicseguros.com, provide insurance coverage to individuals and companies in over 110 countries. We also work as strategic marketing partners with financial institutions to develop incentive-based programs linked to our insurance plans, designed for their private banking level clients.</p>

            <p>GlobalInsuranceNet.com is the first and only interactive insurance website in English designed for the non-US resident or expat. We simplify the insurance buying process by providing real-time comparative quotes, product specifications, policy applications, and helpful consumer advice.</p>

            <p>In addition, we maintain an active network of select insurance brokers throughout the world, to provide our clients with personal service, if they prefer it. This local presence allows us to provide our clients with a truly integrated service capability, allowing them to do as much, or as little, online as they choose.</p>
        </div>
    </div>
</div>

<?php include('templates/footer.php'); ?>