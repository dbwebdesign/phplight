<?php
    $heading = 'Videos';
    include('templates/master.php');
?>

<div class="container">
    <div class="row">
            <?php include('templates/sidebar.php'); ?>
        <div class="col-md-9 text-center">
            <h1 class="text-primary">International Health Insurance 101</h1>
            <iframe width="853" height="480" src="https://www.youtube.com/embed/R8ipNjsexUw" frameborder="0" allowfullscreen></iframe>
            <h1 class="text-primary">Why To Choose Global Insurance Net</h1>
            <iframe width="853" height="480" src="https://www.youtube.com/embed/G4_PLBnb9yU" frameborder="0" allowfullscreen></iframe>
            <h1 class="text-primary">Air Ambulance Services Explained.</h1>
            <iframe width="853" height="480" src="https://www.youtube.com/embed/AB611eX4x0Y" frameborder="0" allowfullscreen></iframe>

        </div>
    </div>
</div>

<?php include('templates/footer.php'); ?>