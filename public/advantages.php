<?php
    $heading = 'Our Advantages';
    include('templates/master.php');
?>

<div class="container">
    <div class="row">
            <?php include('templates/sidebar.php'); ?>
        <div class="col-md-9">
            <iframe width="820" height="461" src="https://www.youtube.com/embed/G4_PLBnb9yU" frameborder="0"
                    allowfullscreen></iframe>

            <h1 class="text-primary">Why Choose Global Insurance Net?</h1>
            <p>Since 1996, Global Insurance Net has provided international health insurance plans to individuals, families and groups all over the world. We are an independent insurance agency, and not affiliated with any insurance company other than as an independent representative. Therefore, our advice to you is 100% objective and free of "hype", and we are determined to get you the best plan at the best price, period. We represent all of the major companies active in this market, so we have a wide variety of plans to fit any need.</p>

            <h1 class="text-primary">Are the Companies You Represent Safe?</h1>
            <p>It's important to do business with reputable companies with the financial backing to pay your claims in a timely manner. We offer you only the finest insurance companies active in the international market, with the highest security ratings available</p>

            <h1 class="text-primary">Don't I Save Money by Going Direct to the Insurance Company? </h1>
            <p>No, our industry is one of the few that offers fixed rates by law that are the same whether you use an agent or not. In other words, "eliminating the middleman" does NOT result in a lower premium, since we are paid a portion of the insurance company's standard fee. If you go direct to them, they simply earn their full fee, but you get nothing in return since insurance regulations require that they treat all of their clients equally, whether they go direct or through an independent agent like us.</p>

            <h1 class="text-primary">What About Claims?</h1>
            <p>In most cases, the bills are paid directly by the insurance company. However, when reimbursements are necessary, we will process your claims for you. Our experienced staff will handle your claims with precision, and help you get reimbursements paid quickly and accurately. This can be difficult to do on your own from abroad.</p>

            <h1 class="text-primary"> What Kind Of Service Do I Get After I Become a Client?</h1>
            <p>Our staff is on hand to help you with emergencies, questions, concerns, advice, etc. This can especially helpful if you are in need of medical care overseas in an unfamiliar environment. Please ask us for references regarding our excellent service.</p>

            <h1 class="text-primary">How Long Does This Take?</h1>
            <p>Once the proper plan is chosen, you can apply online and be approved right away.  If you prefer to apply offline, we will send you the application in a PDF and you can scan and email it to us.  You can also fax your application to us (fax # 305-675-6134). We will process it immediately and get a response from the insurance company within 24-72 hours, depending on the type of plan chosen. Normally, our clients receive their policy and ID cards within 7-10 business days after approval.</p>

            <h1 class="text-primary">To Sum Up:</h1>

            <ul>
                <li> We help you choose the right plan from the right company at the best price.</li>
                <li> We help you through the application and approval process.</li>
                <li> We only work with the highest rated companies in the businesss.</li>
                <li> We will process your claims.</li>
                <li> We provide excellent customer service.</li>
                <li><strong>You get us for free, so you have nothing to lose and much to gain</strong>.</li>
            </ul>
        </div>
    </div>
</div>

<?php include('templates/footer.php'); ?>