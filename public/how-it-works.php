<?php
    $heading = 'How It Works';
    include('templates/master.php');
?>

<div class="container main-body">
    <div class="row">
            <?php include('templates/sidebar.php'); ?>
        <div class="col-md-9">
            <h1 class="text-primary">How Does International Health Insurance Work?</h1>

            <p>International health insurance (sometimes called medical insurance) is a contract between you and an insurance company. In exchange for your premium payments, the insurance company agrees to pay for medical expenses you incur under the terms of the contract.</p>

            <p>Health insurance is essential in this day and age because a single accident or serious illness can wipe out your savings and plunge you into debt.</p>

            <p>There are many reasons to purchase international health insurance for you and your family, but this is the most important one since medical charges can run into the tens of thousands of dollars or more for a single injury or illness. Furthermore, the costs for hospitalization and treatment of injuries continue to rise at a rate that exceeds inflation.</p>

            <p>The international health insurance plans offered by Global Insurance Net eliminate these risks, since you and your family can get unlimited lifetime coverage.</p>

            <h1 class="text-primary">What Do We Get?</h1>

            <p>All of our offered plans cover costs for expenses such as:</p>

            <ul>
                <li>Hospitalization</li>
                <li> Intensive care</li>
                <li> Transplants</li>
                <li> Outpatient services</li>
                <li>  Emergency services</li>
                <li> Emergency medical examination, including air ambulance</li>
                <li> Maternity</li>
                    ...and much more
            </ul>

            <p>Even though you are not a U.S. resident, with any of these plans, you are free to travel to the U.S. to receive care. All of our plans are designed to cover medical costs incurred on a global basis, and we recognize that many of our clients prefer to come to the U.S. for medical care.</p>


            <p>Many hospitals will accept your medical insurance ID card and bill charges directly to your insurance provider. In other cases, you will have to file a claim to get reimbursed.</p>

            <h1 class="text-primary">What About Our Claims?</h1>

            <p>When you are approved for coverage you will receive detailed claims instructions along with your policy and ID card. Our customer service representatives will be available to help you with any questions regarding the claims process. If needed, we will get personally involved between you and your insurance company to help you get your claims paid without delay.</p>


            <?php include('templates/companies.php') ?>
        </div>
    </div>
</div>

<?php include('templates/footer.php'); ?>