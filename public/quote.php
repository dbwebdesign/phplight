<?php
$total_row = 0;
$heading = 'Instant Quotes';
include('templates/master.php');
include ('data/countries.php');
include ('data/quote-engine.php');

?>

<div class="container main-body">
    <div class="row">
          <?php include('templates/sidebar.php'); ?>
        <div class="col-md-9 steps">
            <ul class="breadcrumb">
                <li class="three"><a href="#"><span class="step">1</span>Get Quotes</a></li>
                <li><a href="contact.php"><span class="step">2</span>Contact Us</a></li>
                <li><a href="apply-now.php"><span class="step">3</span>Easily Apply</a></li>
            </ul>

            <div class="col-md-12 text-center quote-heading">
                <h1 class="text-primary">You Do Not Have To Speak To An Insurance Agent To Get Your Quotes!</h1>
                <p>Get instant comparative international health insurance quotes from the top US and European insurance companies. When you are finished, just click on the submit button at the bottom of the page.</p>
            </div>
            <div class="col-md-12">
                <form action="<?php echo $_SERVER['PHP_SELF']; ?>" method="post">
                    <div class="panel panel-warning">
                       <div class="panel-heading text-center">
                            <h3 class="panel-title"><i>Get Instant Quotes! Fill Out Your Details Below.</i></h3>
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-md-5">
                                    <div class="form-group">
                                        <select name="country" class="form-control">
                                            <option value="">Select A Country</option>
                                            <?php do { ?>
                                            <option value="<?php echo  $row_country['id']; ?>"><?php echo  $row_country['name']; ?></option>
                                            <?php } while ($row_country = mysqli_fetch_assoc($countries)); ?>
                                            <?php   mysqli_free_result($countries); ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-5">
                                    <div class="form-group">
                                        <select name="gender" class="form-control">
                                            <option value="">Select Gender</option>
                                            <option value="male">Male</option>
                                            <option value="female">Female</option>
                                        </select>
                                         <?php echo "<p class='text-danger'>$errGender</p>";?>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <input type="text" name="age" class="form-control" placeholder="Age">
                                        <?php echo "<p class='text-danger'>$errAge</p>";?>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class='deductible-title'>
                                        Select Deductible Level(s). (Choose as many as you like) <?php echo "<span class='text-danger'>$errDed</span>";?>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="checkbox quote">
                                        <label class="checkbox-inline">
                                            <input type="checkbox" name="deductible1" value="250"> 250
                                        </label>
                                        <label class="checkbox-inline">
                                            <input type="checkbox" name="deductible2" value="500"> 500
                                        </label>
                                        <label class="checkbox-inline">
                                            <input type="checkbox" name="deductible3" value="1000"> 1000
                                        </label>
                                        <label class="checkbox-inline">
                                            <input type="checkbox" name="deductible4" value="2000"> 2000
                                        </label>
                                        <label class="checkbox-inline">
                                            <input type="checkbox" name="deductible5" value="2500"> 2500
                                        </label>
                                        <label class="checkbox-inline">
                                            <input type="checkbox" name="deductible6" value="5000"> 5000
                                        </label>
                                    </div>
                                </div>
                                <div class="dep-box" id="dep-box">
                                <div class="col-md-12" style="margin-bottom:10px;">
                                    <div class='dependant-title'>
                                        Include Spouse/Partner.
                                    </div>
                                </div>

                                <div class="col-md-5 dependant">
                                <p>Add Spouse/Partner</p>
                                </div>
                                <div class="col-md-5">
                                    <div class="form-group">
                                        <select name="addGender" class="form-control">
                                            <option value="">Select</option>
                                            <option value="male">Male</option>
                                            <option value="female">Female</option>

                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <input type="text" name="addAge" class="form-control" placeholder="Age">
                                    </div>
                                </div>
                                </div>

                                <div class="col-md-12 text-center">
                                    <p style="font-size: 10px;"><i>*Completing the quote information form does not in any way 'bind' coverage. A signed application accepted by an insurer and premium payment are required before coverage is effective or can be bound. All amounts shown are in $ USD.</i></p>
                                </div>
                            </div>
                        </div>
                        <div class="panel-footer clearfix">
                            <button name="quote" type="submit" class="btn btn-warning pull-right">Get Quote!</button>
                        </div>
                    </div>
                </form>
            </div>

            <div class="col-md-12" id="scrollTo">

                <div id="quotes" class="hide">
                   <?php if(count($list) > 0){ ?>
                    <div class="text-center" style="margin-bottom: 20px;">
                        <p style="margin-top: 10px;"><b>**For children's rates please <a href="contact.php">contact us</a> with their ages for customized quotes.</b></p>
                        <p style="margin-top: 10px;"><b>***Ages above 64 require customized quotes, please <a href="contact.php">contact us</a> in that case.</b></p>

                        <a href="contact.php" class="btn btn-warning" style="width: 100%; font-size: 15px;">OK, I Have These Rates - Now tell us more about your needs so that we can help you find the right health plan.
                            <i class="fa fa-chevron-right"></i>
                            <i class="fa fa-chevron-right" style="margin-left: -7px;"></i>
                        </a>
                    </div>

                    <?php foreach ($list as $company => $deductibles) {?>
                        <div class="panel panel-warning">
                            <div class="panel-heading">
                                <h3 class="panel-title"><?php echo $company; ?></h3>
                            </div>
                            <div class="panel-body">
                                <?php foreach ($deductibles as $deductible => $rates) { ?>
                                    <div class="col-md-3">
                                        <ul class="list-group">
                                            <li class="list-group-item list-group-item-warning">Deductible: <?php echo $deductible; ?></li>
                                            <?php foreach($rates as $gender => $rate){ ?>
                                                <li class="list-group-item"><?php echo ucwords($gender).': '.$rate; ?></li>
                                            <?php }?>

                                        </ul>
                                    </div>
                                <?php } ?>
                            </div>
                        </div>
                    <?php } ?>

                    <div class="text-center" style="margin-bottom: 20px;">
                        <a href="contact.php" class="btn btn-warning" style="width: 100%; font-size: 15px;">OK, I Have These Rates - Now tell us more about your needs so that we can help you find the right health plan.
                            <i class="fa fa-chevron-right"></i>
                            <i class="fa fa-chevron-right" style="margin-left: -7px;"></i>
                        </a>
                    </div>
                    <?php } else{ ?>
                   <div class="text-center">
                        <h4 class="text-primary">Sorry, we do not have any rates for the information that you provided. Please check that the information is correct and try again or <a href="contact.php">contact us</a> and we will provide you with customized quotes.  Ages above 64 require customized quotes, please <a href="contact.php">contact us</a> in that case.</h4>
                        <a href="contact.php" class="btn btn-warning" style="width: 100%; font-size: 21px;">Get Custom Quotes!
                            <i class="fa fa-chevron-right"></i>
                            <i class="fa fa-chevron-right" style="margin-left: -10px;"></i>
                        </a>
                    </div>

                 <?php } ?>
                </div>
            </div>

        </div>
        </div>
    </div>
</div>
<?php include('templates/footer.php'); ?>