<?php
    $heading = 'Our Services';
    include('templates/master.php');
?>

<div class="container">
    <div class="row">
            <?php include('templates/sidebar.php'); ?>
        <div class="col-md-9">
            <br>
            <p>Today, millions of people pay too much for their insurance. And millions more, trying to save money, get their insurance from smaller companies that offer only limited benefits.  However, with our guidance, you can find truly comprehensive coverage at an affordable price.</p>

            <h1 class="text-primary">Why Spend More Than You Have For Your Insurance?</h1>
            <p>We are committed to finding you the highest quality international health insurance cover at the lowest possible price, while providing the highest level of customer service.</p>

            <h1 class="text-primary">Why Pay Money To Insurance Companies That You Can't Rely On?</h1>
            <p>We only offer plans issued and backed by the finest, most reliable U.S. insurance companies. Many of our current clients came to us after their previous insurance provider refused to pay their claims. Don't make the same mistake.</p>


            <h1 class="text-primary">We Are Truly Objective.</h1>
            <p>At Global Insurance Consultants, we are not owned, employed, or controlled by any insurance company, so we can remain totally independent, objective and loyal to you. Our services are always free to you as well.</p>

            <h1 class="text-primary">We Have The Best Prices Available.</h1>
            <p>We are compensated by the insurance companies that we represent so you never pay a fee for our services.  Your premium would be exactly the same if you tried to buy from the insurance carrier directly. We make it simple and convenient and give you more, so you have nothing to lose and everything to gain when you buy through us.</p>

            <?php include('templates/companies.php'); ?>
        </div>
    </div>
</div>

<?php include('templates/footer.php'); ?>