<img src="/resources/img/companies/cigna.jpg" alt="Cigna">
<h1 class="text-primary">About Cigna</h1>
<p>Cigna is one of the largest health service organizations in the United States. Our mission is to help the people we serve improve their health, wellbeing and sense of security. Cigna operating subsidiaries provide an integrated suite of medical, dental, behavioral health, pharmacy and vision care benefits, as well as group and individual life, accident and disability insurance, to millions of people throughout the United States and around the world.</p>

<p>From our roots in the US cities of Philadelphia, Pennsylvania and Hartford, Connecticut, Cigna has grown to become a global organization. Today, we have 60 million customer relationships across the world and support this with a worldwide staff of 30,000 employees.</p>

<h1 class="text-primary">About Cigna International</h1>
<p>Cigna International provides access to superior quality healthcare and related financial protection programs to employers, affinity groups and individuals around the globe.</p>
<p>Leaders in Global Healthcare Benefits With more than 30 years international experience, Cigna is the leader in providing global healthcare benefits to multi-national businesses. In fact, we cover over 700,000 expatriates for more than 2,000 clients.</p>

<h1 class="text-primary">We offer:</h1>
<ul>
    <li>Individual international healthcare plans</li>
    <li>Corporate international plans</li>
    <li>Corporate travel plan</li>
    <li>Dental and vision plans</li>
    <li>International employee assistance program</li>
    <li>Continuation options</li>
    <li>Wellbeing programs</li>
</ul>