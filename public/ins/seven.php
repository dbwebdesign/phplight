<img src="/resources/img/companies/seven-corners.jpg" alt="Seven Corners">
<p>The Reside Prime plan from Seven Corners that we offer on this site is underwritten by Certain Underwriters at Lloyd's of London and is rated A "Excellent" by A.M. Best. In addition to being one of the largest insurance entities in the world, Lloyd's has over 300 years of experience in the international insurance business.</p>

<h1 class="text-primary">Background</h1>
<p>Seven Corners began offering international medical insurance to the worldwide community in 1993. International insurance professionals with a strong commitment to customer service started Seven Corners to provide the public with a reliable option for the global insurance needs.</p>

<p>Prior to 1993, the quality of international insurance was questionable at best . At the time, the majority of companies offering such coverage were offshore (or foreign) insurance carriers who wrote international medical as more of a side business rather than as a true, legitimate segment of the market.</p>

<p>While many of these companies did (and still do) have a rating from an accredited company, the insurance carrier is still based in a foreign country and is often not susceptible to the insurance laws of the United States. Since inception, Seven Corners has sought to use domestic, "A" rated insurance companies wherever possible.</p>

<h1 class="text-primary">Who Is Seven Corners?</h1>
<p>Within the insurance industry, Seven Corners is considered a Managing General Underwriter. We administer applications, process claims, pay agent commissions, etc., but we do not take any of the risk - we leave that responsibility to the experts.</p>
<p>The advantage with using Seven Corners is that you receive all of the benefits of a quality product without any of the bureaucracy common with large insurance companies.</p>

<h1 class="text-primary">The People Involved</h1>
<p>In the pursuit of serving our customers, Seven Corners utilizes the talents of its:</p>

<ul>
    <li><b>Underwriters</b> - Professionals who's job it is to review the applications of our perspective clients and attempt to find a way of insuring individuals and families with a wide variety of health histories. </li>
    <li><b>Customer Service / Administration</b> - This category is combined for one reason, because the service we perform cannot be separated from how we perform that job. Our administrative personal are the ones speaking with insureds as well as agents, so they are the first to realize that our continued success relies on efficiency and service to our customers. </li>
    <li><b>Claims</b> - When the unexpected does occur overseas, our staff is prepared to address those medical claims submitted to us efficiently and directly as possible. When dealing with a medical situation while outside of your home country, you don't want to rely on a company whose primary objective is delay and mislead. Seven Corners attempts to ensure that your requests for claim reimbursements are dealt with as quickly as possible.</li>
</ul>