<img src="/resources/img/companies/img.jpg" alt="International Medical Group">
<p>International Medical Group, Inc. (IMG) truly understands the needs of international citizens. For more than a decade, IMG has provided medical insurance to individuals, families and groups who are living or traveling abroad. Whether you need individual coverage for a vacation or group coverage for your employees in locations around the world, IMG has a product to meet your needs.</p>

<p>IMG's strength lies in its ability to deliver superior service to insured members. The staff includes international claims administrators with a wealth of experience in processing claims from throughout the world; multilingual customer service representatives to assist members; and an on-site medical staff available 24 hours a day, seven days a week for medical emergencies. </p>

<p>IMG does not sell directly, relying instead on a network of agents to promote its insurance products. They maintain an expert staff of marketing professionals to recruit, educate and assist the contracted agents.</p>

<p>When deciding which company will insure your health, there are many important factors to consider. In addition to comprehensive benefits and experienced administration, there must be the commitment and financial stability of an established international insurance company.</p>

<p>While IMG provides complete plan administration expertise, our globally recognized underwriter, Sirius International Insurance Corporation (publ), offers the financial security and reputation demanded by international consumers. Rated A (excellent) by A.M. Best and A- by Standard & Poor's*, Sirius International shares IMG's vision of the international marketplace and offers the stability of a well-established insurance company. Sirius International is a White Mountains Re company.</p>

<p>Growing year by year, expanding globally, building upon a solid reputation, remaining stable but never standing still - these characteristics make IMG and Sirius International the team to choose for your Global Peace of Mind®.</p>

<p>*Sources: A.M. Best affirmed their rating in a press release dated July 31, 2009; Standard & Poor's affirmed their rating in a press release dated December 9, 2009. Ratings are accurate as of the date of publishing and are subject to change.</p>