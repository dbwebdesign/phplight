<img src="/resources/img/companies/hth.jpg" alt="HTH Worldwide">
<p>Founded in 1997, HTH Worldwide has grown to become a leading provider of international health insurance programs and an innovator in online healthcare information, medical assistance and insurance services around the globe. HTH offers a full range of individual and group specialty health insurance programs, all of which include unique Global Health and Safety Services.</p>

<h1 class="text-primary">Markets Served</h1>
<p>All HTH markets share the same unmet need – travelers and expatriates inevitably experience medical problems abroad and have extraordinary difficulty arranging quality healthcare services or resolving emergency medical situations. The lack of knowledge of local physicians and hospitals is compounded by insurance coverage that shrinks or disappears when international borders are crossed.</p>

<h1 class="text-primary">Complete Healthcare Solution</h1>
<p>HTH combines insurance with services designed to help members identify and access quality healthcare, worldwide. After all, what good is insurance if you can’t find a doctor or hospital you can trust?</p>

<p>HTH Members enjoy access to our community of contracted physicians in more than 170 countries, online physician profiles, interactive translation guides for brand name pharmaceuticals and common medical terms and phrases, featured articles on dozens of travel-health topics and up-to-date health and security news. HTH administers its insurance programs and provides exceptional service via its online tools and 24/7 toll-free multilingual customer support.</p>

<h1 class="text-primary">Strength of a U.S. Insurer</h1>
<p>Over 500,000 international travelers and expatriates are covered annually by HTH’s products and services. Our plans are exclusively insured by U.S insurance carriers. Our health plans meet standards set by state regulators and feature coverage more generous than plans sold as “surplus lines” coverage. Selling an admitted “U.S policy” protects our members and affords them important rights such as a right to appeal claims decisions and insurance regulator oversight.</p>

<p>Our Global Citizen health plan is insured by HM Life Insurance Company (A- Excellent) and Unicare Life and Health Insurance Company- (A- Excellent). HTH Worldwide Insurance Services is a fully licensed insurance broker and administrator and a subsidiary of HTH Worldwide. The company maintains its home office in Radnor, PA, USA.</p>