<img src="/resources/img/companies/geoblue-logo.jpg" alt="GeoBlue">

<p>GeoBlue is a leader and innovator serving the needs of world travelers. GeoBlue Expatsm and GeoBlue Travelersm group health plans combine unsurpassed service and mobile technology to help you access trusted doctors and hospitals all around the globe. GeoBlue is a trade name of Worldwide Insurance Services, LLC, an independent licensee of the Blue Cross Blue Shield Association.</p>

<h1 class="text-primary">International Group Health Plans You Can Trust</h1>
<p>GeoBlue offers a full range of group plans with comprehensive benefits and competitive rates. All plans are distinguished by the highest standards of service delivering:</p>
<ul>
    <li>Appointment scheduling for doctor visits with no out-of-pocket costs</li>
    <li>Outpatient care coordination, especially for chronic conditions and special needs</li>
    <li>Inpatient case management and emergency evacuation</li>
</ul>
<p>GeoBlue members enjoy access to a carefully selected global provider community including the BlueCard network while in the U.S.</p>
<p>Insurance benefits are underwritten by 4 Ever Life Insurance Company, an independent licensee of the Blue Cross Blue Shield Association</p>
<p>4 Ever Life is rated "A-" (Excellent) by A.M. Best and licensed to provide health and life insurance solutions in all 50 states, the District of Columbia, and Puerto Rico.</p>




