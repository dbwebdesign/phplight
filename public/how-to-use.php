<?php
    $heading = 'How To Use Our Site';
    include('templates/master.php');
?>

<div class="container">
    <div class="row">
            <?php include('templates/sidebar.php'); ?>
        <div class="col-md-9">
            <div class="use-site">
                <h2>Step 1</h2>
                <h1 class="text-primary">Find The Right Type of Plan</h1>
                <p>First, you need to decide if you require long term or short term coverage. If you are planning on spending MORE than 12 months out of your home country you will need a long term international health insurance plan. If you will spend a year or LESS overseas, or if you are a non-US citizen and are visiting or recently moved to the US, you need a travel medical insurance plan.  In either case, simply <a href="contact.php" style="color: red;">contact us</a> with your details so that we can help you identify exactly which type of plan you need.</p>
            </div>

            <div class="use-site">
                <h2>Step 2</h2>
                <h1 class="text-primary">Compare Rates and Coverage Details</h1>
                <p>Next, you need to see what the various plans will cost and how the coverages compare. If you need a long term international health insurance plan, <a href="quote.php" style="color: red;">click here</a> to get side-by-side cost comparisons from the top providers of these plans. To see the benefits provided, <a href="contact.php" style="color: red;">contact us</a> with your details so that we can send information and benefits on the best plan for your specific needs.  For short term travel medical insurance plans, <a href="contact.php" style="color: red;">contact us</a> so that we can send you information and rates. </p>
            </div>

            <div class="use-site last">
                <h2>Step 3</h2>
                <h1 class="text-primary">Apply or Request an Application</h1>
                <p> Lastly, you will need to apply for coverage. For both long and short term international health insurance plans you can apply online and secure your coverage right away. <a href="apply-now.php" style="color: red;">Click here</a> for the online applications.  If you prefer a pdf application just let us know and we will email or fax it to you.</p>
            </div>


        </div>
    </div>
</div>

<?php include('templates/footer.php'); ?>