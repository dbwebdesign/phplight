<?php
    $heading = 'Our Team';
    include('templates/master.php');

?>

<div class="container">
    <div class="row">
            <?php include('templates/sidebar.php'); ?>
        <div class="col-md-9">
            <div class="profile">
                <img src="resources/img/employees/carlos.jpg" alt="Carlos Perez">
                <h1 class="text-primary">Carlos Perez - President, CEO</h1>
                <p>Global Insurance Net is owned and managed by Carlos Perez who has an extensive track record in the international financial services industry.  Since 1983, Mr. Perez has held senior level positions at prestigious financial firms such as Chase Manhattan Bank, Merrill Lynch and Shearson Lehman Brothers with a focus throughout his career on expats and foreign nationals. He obtained his first insurance license in 1984 and has been active in the international insurance and investment industry since then.  He founded Global Insurance Consultants, Inc. in 1996 and has represented many of the largest insurers such as Aetna, AIG, BlueCross BlueShield, CNA, New York Life and many more.</p>
                <br>
                <p><b>His accomplishments include:</b></p>
                <br>
                <ul>
                    <li> Chairman's Council, Shearson Lehman Brothers (a division of American Express)</li>
                    <li>International Mutual Fund Advisory Committee, Shearson Lehman Brothers</li>
                    <li>He received his B.A. in Economics from Clemson University and has various licenses for
                        securities and insurance.</li>
                </ul>
            </div>

            <div class="profile">
                <img src="resources/img/employees/ville.jpg" alt="Carlos Perez">
                <h1 class="text-primary">Ville Mehtonen - Head of Business Development</h1>
                <p>Born in Kuopio, Finland, Ville Mehtonen graduated with a B.A. from the University of South Florida in 2012. Since 2015 he has been living in Prague, Czech Republic. Previously an employee of Accenture PLC management consulting company, Ville joined the Global Insurance Net team in 2016, has over five years experience working in business development, client service; and is fluent in Finnish and proficient in Swedish.</p>
            </div>

            <div class="profile">
                <img src="resources/img/employees/monica.jpg" alt="Carlos Perez">
                <h1 class="text-primary">Monica Perez - Client Services Representative</h1>
                <p>Monica Perez graduated with a B.A. from the University of South Florida in 2013.  Ms. Perez has years of experience in client service working for such internationally recognized companies as Benihana Inc. and eBay Inc.  Since 2008 she has been an indispensable representative on behalf of Global Insurance Net's client service.</p>
            </div>
        </div>
    </div>
</div>

<?php include('templates/footer.php'); ?>