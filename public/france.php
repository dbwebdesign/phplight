<?php
    $heading = 'Going To France';
    include('templates/master.php');
?>

<div class="container">
    <div class="row">
            <?php include('templates/sidebar.php'); ?>
        <div class="col-md-9">
            <h1 class="text-primary">INTERNATIONAL HEALTH INSURANCE FOR FRENCH VISA</h1>
            <p><b>QUICK AND EASY</b> - Compare affordable International Health Insurance plans which meet the requirements for the French Visa – <a href="quote.php">click here!</a> </p>
            <img src="/resources/img/france.jpg" alt="">
            <p><b>We can help</b> - we know how to simplify the Visa application process for you !</p>
            <p>We provide personalized service and useful advice on the proper plan for your specific needs and budget. We have many years of experience in this area and are proud to offer several international health insurance plans which surpass the minimum requirements for the French Visa. For more details on the French Visa requirements see below.</p>
            <p><b>Visa for France</b> – We have helped many clients obtain a global health insurance plan which is required for a “Long Stay Visa” for France (for stays over 90 days). The official requirement is “proof of medical insurance with coverage valid in France”, which is precisely what our plans offer. Once you are approved you will receive an official verification of coverage document which you need to include with your visa application. In some cases, we are able to provide proof of insurance within 24 hours after receiving an application!</p>

            <div class="text-center"  style="border-bottom: 2px solid #337ab7; margin-bottom: 10px;">
                <a href="quote.php" class="btn btn-warning">GET INSTANT COMPARATIVE QUOTES</a>
                <h1 class="text-primary">LONG STAY VISAS FOR FRANCE (for stays over 90 days)</h1>
            </div>

            <p>A visa is required for a long stay in France, in a French Overseas Department or Territory, or in Monaco.</p>
            <p><b>REQUIREMENTS:</b></p>
            <p>Passport signed and valid 3 months after the last day of stay + 3 copies (The consulate will not keep the passport, only the copies) Four application forms (three for accompanying children ) signed and legibly filled out. Four recent passport-size photographs ( three for children ) glued on the forms. A proof of residency status in the US for non-US citizens. Visitors in the US with a B1/B2 visa and holders of re-entry permits cannot apply for long-stay visas. Financial guarantee such as:</p>

            <ul>
                <li>Formal letter of reference from the applicant's bank showing account numbers and balances or recent bank, savings or brokerage account statements + 3 copies</li>
                <li>For people wishing to retire in France, proof of sufficient income: pension, dividends, savings, band and brokerage account statements + 3 copies</li>
                <li>Affidavit of support from a sponsor in France along with evidence of financial means such as paystubs or income tax returns + 3 copies and proof of adequate housing +

                    <div class="box-danger name">
                        <div class="box-body">
                            <p><i class="fa fa-quote-left" aria-hidden="true"></i> 3 copies Proof of medical insurance with coverage valid in France</p>
                        </div>
                    </div>


                    + 3 copies Police clearance: document obtained from the Police Department of the place of residence in the United States stating that the applicant has no criminal record + 3 copies Letter from applicant certifying that he/she will not have any paid activity in France + 3 copies</li>
            </ul>
            <p>The applicant must make an appointment with the Visa Section for an interview. In Arizona, Colorado, New Mexico and Southern Nevada, the interview can be arranged with the Honorary Consuls. The processing time is about six weeks. : the visa fee is paid when the visa is issued.</p>

            <div class="text-center" style="border-bottom: 2px solid #337ab7; margin-bottom: 10px;">
                <h1 class="text-primary">LONG STAY STUDENT VISAS FOR FRANCE</h1>
            </div>

            <p><b>Please, read GENERAL INFORMATION before applying.</b></p>
            <p>There are two kinds of long-stay student visas available, depending upon the length of studies in France as indicated in the letter of enrollment.</p>
            <ol>
                <li>temporary long stay visas: from 3 to 6 months, multiple entries, no "carte de séjour" to request but no possibility to extend the stay,</li>
                <li>One year-visas (for stays over 6 months): the visa is valid for 1 entry during a specific three-month period. After arriving in France and preferably within a week, the students must request their "carte de séjour" at the Préfecture. At this point, the students will have to undergo a medical check-up by a doctor from the Office des Migrations Internationales.</li>
            </ol>
            <p>For stays up to 3 months, the student should apply for a Schengen visa, multiple entries, and should use the short stay visa application form. The visa fee will be half the regular Schengen fee and the documents to present will be the same as for the long-stay visa: proof of enrollment, evidence of medical insurance and financial support.</p>
            <p>Non US citizens need to contact the Consulate to find out about extra requirements and processing time.</p>

            <p><b>REQUIREMENTS (for students over 18):</b></p>
            <p>Passport signed and valid for a period of three months beyond the applicant's last day of stay in France + 1 copy Two long stay visa application forms legibly filled out and signed Two passport-size photographs, one glued on each form A letter of admission (pre-enrollment) from the school which the applicant plans to attend to in France + 1 copy Financial guarantee such as a notarized statement certifying that the applicant will be provided with a monthly allowance of $600 for the duration of the stay in France, or a proof of personal income along with a letter from the school stating that room, board and tuition are fully prepaid + 1 copy.</p>

            <p><b>Medical insurance:</b></p>
            <ul>
                <li>stay up to 6 months: when applying for a visa, the students should present a letter from their insurance company stating that the coverage is valid in France (+ 1 copy)</li>
                <li>stay over 6 months: after their arrival in France, the students under 28 years old and enrolled in a French school affiliated to the French Social Security, must join this social security for the length of their studies. This is verified by the Prefecture when the students apply for the residency card. The reimbursement of the medical expenses amounts to 70%. For the 30% remaining, these students have the choice to join a French student insurance OR make use of their own insurance with worldwide coverage (letter from the insurance company). However, the students over 28 years old, or enrolled in a French school not affiliated to the French Social Security, cannot enroll in the French Social Security and must show a proof of insurance with coverage valid in France when they apply for the visa and for the residency card. "Student" visa fee in cash or by credit card</li>
            </ul>
            <p>IF UNDER 18 at the date of arrival in France, the processing times is 4 to 6 weeks. Please contact the Consulate and ask for the special visa application packet for minors.</p>
            <p>RECIPIENTS OF FRENCH GOVERNMENT, FOREIGN GOVERNMENT OR INTERNATIONAL ORGANIZATIONS SCHOLARSHIPS: A Scholarship recipient only needs to present the first three items on the above list, plus the letter awarding the scholarship + 1 copy. The applicant will also have to go for a medical checkup by a doctor accredited by the Consulate. The medical checkup is paid by the applicant but the visa is free of charge.</p>
        </div>
    </div>
</div>

<?php include('templates/footer.php'); ?>