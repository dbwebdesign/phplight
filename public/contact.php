<?php
    $heading = 'Contact Us';
    include('templates/master.php');
    include('data/process.php');
?>

<div class="container">
    <div class="row">
            <?php include('templates/sidebar.php'); ?>
        <div class="col-md-9">
            <ul class="breadcrumb steps">
                <li><a href="quote.php"><span class="step">1</span>Get Quotes</a></li>
                <li class="three"><a href="#"><span class="step">2</span>Contact Us</a></li>
                <li><a href="apply-now.php"><span class="step">3</span>Easily Apply</a></li>
            </ul>
                    <form class="form-horizontal" role="form" method="post" action="contact.php">
                        <div class="form-group">
                            <div class="col-sm-12">
                                <?php echo $result; ?>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-12 contact-checkbox">
                                <div class="bg-primary text-center" style="padding: 2px;">Which international health insurance plan would you like more information about? If you are not sure, let us help you decide.</div>
                                <div class="checkbox" style="border: 1px solid #337ab7; padding: 10px;">
                                    <label style="color: red;">
                                        <input type="checkbox" name="ins[]" value="Not Sure"> Not sure, I need help deciding which health plan is best for me.
                                    </label><br>
                                    <label class="checkbox-inline">
                                        <input type="checkbox" name="ins[]" value="Azimuth Risk Solutions">Azimuth Risk Solutions
                                    </label>
                                    <label class="checkbox-inline">
                                        <input type="checkbox" name="ins[]" value="Cigna"> Cigna
                                    </label>
                                    <label class="checkbox-inline">
                                        <input type="checkbox" name="ins[]" value="GeoBlue"> GeoBlue
                                    </label>

                                    <label class="checkbox-inline">
                                        <input type="checkbox" name="ins[]" value="HCC"> HCC
                                    </label>
                                    <label class="checkbox-inline">
                                        <input type="checkbox" name="ins[]" value="HTH"> HTH
                                    </label>

                                    <label class="checkbox-inline">
                                        <input type="checkbox" name="ins[]" value="IMG"> IMG
                                    </label>
                                    <label class="checkbox-inline">
                                        <input type="checkbox" name="ins[]" value="Seven Corners"> Seven Corners
                                    </label>
                                </div>
                            </div>
                        </div>

                        <div class="form-wrap" style="border: 1px solid #337ab7; margin-top: -15px; margin-bottom:
                        20px;">
                            <div class="bg-primary text-center">About You</div>
                        <div class="form-info" style="padding: 10px;">
                            <div class="form-group">
                                <div class="col-sm-12">
                                    <div class="input-group">
                                        <div class="input-group-addon">
                                            <span class="glyphicon glyphicon-user"></span>
                                        </div>
                                        <input class="form-control" id="name" name="name" type="text" placeholder="First & Last Name" value="<?php echo htmlspecialchars($_POST['name']); ?>"/>
                                        <?php echo "<p class='text-danger'>$errName</p>";?>
                                    </div>
                                </div>
                            </div>

                             <div class="form-group">
                                <div class="col-sm-6">
                                    <div class="input-group">
                                        <div class="input-group-addon">
                                            <span class="glyphicon glyphicon-envelope"></span>
                                        </div>
                                        <input class="form-control" id="email" name="email" type="text"placeholder="example@domain.com" value="<?php echo htmlspecialchars($_POST['email']); ?>">
                                        <?php echo "<p class='text-danger'>$errEmail</p>";?>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="input-group">
                                        <div class="input-group-addon">
                                            <span class="glyphicon glyphicon-map-marker"></span>
                                        </div>
                                        <input type="text" class="form-control" id="country" name="country" placeholder="Country Of Nationality" value="<?php echo htmlspecialchars($_POST['country']); ?>">
                                        <?php echo "<p class='text-danger'>$errCountry</p>";?>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-sm-12">
                                    <p style="color: red;">The more we know about your specific needs the better we can help you. Please give us as much information as you can regarding the country(s) that you will move or travel to / or are currently residing in, how long you will need health insurance cover, any specific medical conditions, etc.</p>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-12">
                                    <div class="input-group">
                                        <div class="input-group-addon">
                                            <span class="glyphicon glyphicon-pencil"></span>
                                        </div>
                                        <textarea class="form-control contact-message" rows="4" name="message"><?php echo htmlspecialchars($_POST['message']);?></textarea>
                                        <?php echo "<p class='text-danger'>$errMessage</p>";?>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-sm-6">
                                    <div class="input-group">
                                        <div class="input-group-addon">
                                            <span>2+3 =</span>
                                        </div>
                                        <input type="text" class="form-control" id="human" name="human" placeholder="Your Answer">
                                        <?php echo "<p class='text-danger'>$errHuman</p>";?>
                                    </div>
                                </div>
                                <div class="col-sm-2 text-right">
                                    <input id="reset" name="reset" type="reset" value="Reset Form" class="btn btn-danger
                                form-control">
                                </div>
                                <div class="col-sm-4 text-right">
                                    <input id="submit" name="submit" type="submit" value="Submit Form" class="btn
                                btn-primary form-control">
                                </div>
                            </div>

                        </div>
                        </div>
                    </form>
        </div>
    </div>
</div>

<?php include('templates/footer.php'); ?>