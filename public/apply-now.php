<?php
    $heading = 'Apply Now';
    include('templates/master.php');
?>

<div class="container">
    <div class="row">
            <?php include('templates/sidebar.php'); ?>
        <div class="col-md-9 steps">
            <ul class="breadcrumb">
                <li><a href="quote.php"><span class="step">1</span>Get Quote</a></li>
                <li><a href="contact.php"><span class="step">2</span>Contact Us</a></li>
                <li class="three"><a href="#"><span class="step">3</span>Easily Apply</a></li>
            </ul>
            <h1 class="text-primary">Apply Now For Medical Insurance!</h1>
            <p>Please find links to the online applications below for the insurance plan of your choice. If you prefer a printed application or do not see a link below, please
                <a href="contact.php">Contact Us</a> to let us know which application(s) we should email or fax to you.</p>
            <div class="text-center">
                <a href="contact.php" class="btn btn-warning" style="width: 100%; font-size: 21px;">Have Questions? Contact Us Now!
                    <i class="fa fa-chevron-right"></i>
                    <i class="fa fa-chevron-right" style="margin-left: -10px;"></i>
                </a>
            </div>
            <div class="table-responsive" style="margin-top: 20px;">
                <table class="table table-bordered table-striped">
                    <thead>
                        <tr class="bg-warning">
                            <th>Company</th>
                            <th>Plan Name</th>
                            <th>Application</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>Azimuth</td>
                            <td>Meridian</td>
                            <td><a href="https://www.azimuthrisk.com/?reference_id=a7d13e77">Apply Now <i class="fa fa-arrow-circle-right"></i></a></td>
                        </tr>
                        <tr>
                            <td>Cigna</td>
                            <td>Global Medical</td>
                            <td><a href="https://www.cignaglobal.com/quote/pages/quote/PersonalInformationLiteV3.html?AffinityPartner=5e4b1f8a23a7373f8285820bb3e5f04a&utm_source=broker&utm_medium=tlink&utm_campaign=US10469067">Apply Now <i class="fa fa-arrow-circle-right"></i></a></td>
                        </tr>
                        <tr>
                            <td>GeoBlue</td>
                            <td>Xplorer</td>
                            <td><a href="http://www.geobluetravelinsurance.com/products/longterm/xplorer-5-overview.cfm?link_id=108347">Apply Now <i class="fa fa-arrow-circle-right"></i></a></td>
                        </tr>
                        <tr>
                            <td>HCC</td>
                            <td>Atlas</td>
                            <td><a href="https://quote.hccmis.com/atlastravel/?referid=99972&language=en-US">Apply Now <i class="fa fa-arrow-circle-right"></i></a></td>
                        </tr>
                        <tr>
                            <td>HTH Worldwide</td>
                            <td>Global Citizen</td>
                            <td><a href="http://www.hthtravelinsurance.com/prdCobrand/purchase/quote/quote_ctzn.cfm?link_id=25038&personalized=y&header=y">Apply Now <i class="fa fa-arrow-circle-right"></i></a></td>
                        </tr>
                        <tr>
                            <td>IMG</td>
                            <td>Global Medical</td>
                            <td><a href="https://www.imglobal.com/travelinsurance/index.cfm?imgac=177944&show=GMI">Apply Now <i class="fa fa-arrow-circle-right"></i></a></td>
                        </tr>
                    </tbody>
                </table>

            </div>

        </div>
    </div>
</div>

<?php include('templates/footer.php'); ?>